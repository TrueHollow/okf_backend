#  OKF_PROJECTS

## System requirements:

Node.js version 10 or higher. 

## Prepare

`npm i --production`

## Run

`npm start`

## Support environment files with [`dotenv`](https://www.npmjs.com/package/dotenv#rules)

You can use a `.env` file to set environment variables:
```dotenv
BACKBLAZE_KEY=some_key
BACKBLAZE_KEY_ID=some_key_id

```
