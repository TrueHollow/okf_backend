require('./common/dotEnv_proxy');
const Koa = require('koa');
const config = require('./config');
const { sequelize } = require('./db');
const logger = require('./logger')('src/index.js');

sequelize.sync().then(() => {
  logger.debug('DB sync');
});

logger.info('App started.');

const app = new Koa();

// trust proxy
app.proxy = true;

const middleware = require('./middleware');

middleware(app);

const router = require('./api/v1')();

app.use(router.routes()).use(router.allowedMethods());

app.use((ctx, next) => {
  if (ctx.isAuthenticated()) {
    return next();
  }
  return ctx.redirect('/');
});

const port = process.env.PORT || config.app.PORT;

app.on('error', (err, ctx) => {
  logger.error('server error', err, ctx);
});

const server = app.listen(port, () => logger.info(`Listening port ${port}`));

module.exports = {
  server,
};
