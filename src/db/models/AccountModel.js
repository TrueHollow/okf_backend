const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');

const SALT_ROUNDS = 10;

module.exports = ({ sequelize }) => {
  const AccountModel = sequelize.define('accounts', {
    userName: { type: Sequelize.STRING, allowNull: false },
    email: { type: Sequelize.STRING, allowNull: false, unique: true },
    password: { type: Sequelize.STRING(60), allowNull: false },
    profileImageUrl: { type: Sequelize.STRING, allowNull: true },
    isActive: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: true },
    activationUid: { type: Sequelize.UUID, allowNull: true },
  });

  AccountModel.prototype.calcHashes = async function(password) {
    const currentPassword = password || this.password;
    const passwordHash = await bcrypt.hash(currentPassword, SALT_ROUNDS);
    this.password = passwordHash;
    return passwordHash;
  };

  AccountModel.prototype.checkPassword = async function(otherPassword) {
    return bcrypt.compare(otherPassword, this.password);
  };

  return AccountModel;
};
