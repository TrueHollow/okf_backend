const Sequelize = require('sequelize');
const config = require('../config');
const logger = require('../logger')('src/db/index.js');

const { database } = config;
if (database.logging === true) {
  database.logging = message => {
    logger.debug(message);
  };
}

const sequelize = new Sequelize(database);

const db = {
  sequelize,
};

db.AccountModel = require('./models/AccountModel')(db);

module.exports = db;
