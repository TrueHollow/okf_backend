const logger = require('../../../logger')('src/api/v1/Account/get.js');

module.exports = backend => {
  return async ctx => {
    try {
      const { user } = ctx.state;
      const data = ctx.request.body;
      const account = backend.getAccount();
      const { result, err } = await account.update(data, user);
      if (err) {
        ctx.status = 401;
        ctx.body = { err };
      } else {
        ctx.status = 200;
        ctx.body = { result };
      }
    } catch (e) {
      logger.error(e);
      ctx.status = 500;
      ctx.body = { err: 'Internal error' };
    }
  };
};
