const logger = require('../../../logger')('src/api/v1/Account/postCreate.js');

module.exports = backend => {
  return async ctx => {
    try {
      const data = ctx.request.body;
      const account = backend.getAccount();
      const { result, err } = await account.create(data);
      if (err) {
        ctx.status = 400;
        ctx.body = { err };
      } else {
        ctx.status = 201;
        ctx.body = { result };
      }
    } catch (e) {
      logger.error(e);
      ctx.status = 500;
      ctx.body = { err: 'Internal error' };
    }
  };
};
