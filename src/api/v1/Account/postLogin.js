const logger = require('../../../logger')('src/api/v1/Account/postLogin.js');

module.exports = backend => {
  return async ctx => {
    try {
      const data = ctx.request.body;
      const account = backend.getAccount();
      const { result, err } = await account.login(data);
      if (err) {
        ctx.status = 401;
        ctx.body = { err };
      } else {
        ctx.status = 200;
        ctx.body = { result };
      }
    } catch (e) {
      logger.error(e);
      ctx.status = 500;
      ctx.body = { err: 'Internal error' };
    }
  };
};
