const logger = require('../../../logger')('src/api/v1/Account/getActivate.js');

module.exports = backend => {
  return async ctx => {
    try {
      const { uid } = ctx.params;
      const account = backend.getAccount();
      const { result, err } = await account.activate(uid);
      if (err) {
        ctx.status = 400;
        ctx.body = { err };
      } else {
        ctx.status = 200;
        ctx.body = { result };
      }
    } catch (e) {
      logger.error(e);
      ctx.status = 500;
      ctx.body = { err: 'Internal error' };
    }
  };
};
