const passport = require('koa-passport');
const config = require('../../../config');
const getAccount = require('./get');
const getActivate = require('./getActivate');
const postCreate = require('./postCreate');
const postLogin = require('./postLogin');
const putUpdate = require('./putUpdate');

module.exports = (router, backend) => {
  router.get(
    '/api/v1/account',
    passport.authenticate('bearer', config.jwt.session),
    getAccount(backend)
  );
  router.post('/api/v1/account', postCreate(backend));
  router.put(
    '/api/v1/account',
    passport.authenticate('bearer', config.jwt.session),
    putUpdate(backend)
  );
  router.post('/api/v1/account/login', postLogin(backend));
  router.get('/api/v1/account/activate/:uid', getActivate(backend));
};
