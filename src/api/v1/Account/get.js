const logger = require('../../../logger')('src/api/v1/Account/get.js');

module.exports = () => {
  return async ctx => {
    try {
      const { user } = ctx.state;
      const accountObj = user.toJSON();
      delete accountObj.password;
      ctx.body = accountObj;
    } catch (e) {
      logger.error(e);
      ctx.status = 500;
      ctx.body = { err: 'Internal error' };
    }
  };
};
