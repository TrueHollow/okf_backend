const Account = require('../../service/Account');

class BackendV1 {
  constructor() {
    this.account = new Account();
  }

  getAccount() {
    return this.account;
  }
}

module.exports = BackendV1;
