const Router = require('koa-router');
const Account = require('./Account');
const Upload = require('./Upload');
const BackendV1 = require('./BackendV1');

module.exports = (backend = new BackendV1()) => {
  const router = new Router();

  router.get('/', ctx => {
    ctx.body = {
      name: 'Backend',
    };
  });

  Account(router, backend);
  Upload(router, backend);
  return router;
};
