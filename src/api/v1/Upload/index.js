const crypto = require('crypto');
const passport = require('koa-passport');
const multer = require('@koa/multer');
const imageType = require('image-type');
const sharp = require('sharp');
const logger = require('../../../logger')('src/api/v1/Upload/index.js');
const B2 = require('../../../common/B2');

const sharpOptions = {
  jpeg: {
    quality: 75,
    progressive: true,
  },
  png: {
    progressive: true,
  },
};

const storage = multer.memoryStorage();
const limits = {
  fieldSize: 5 * 1024 * 1024, // 5 Mb
};
const upload = multer({ storage, limits });
const config = require('../../../config');

const processBuffer = async (buffer, id, ext) => {
  const hash = crypto.createHash('sha256');
  hash.update(buffer);
  const hex = hash.digest('hex');
  const fileName = `${hex}.${ext}`;
  let file = sharp(buffer);
  switch (ext) {
    case 'jpeg':
      file = file.jpeg(sharpOptions.jpeg);
      break;
    case 'png':
      file = file.png(sharpOptions.jpeg);
      break;
    default:
  }

  const outputBuffer = await file
    .resize(1024, 768, {
      fit: 'outside',
    })
    .toBuffer();
  return {
    fileName,
    outputBuffer,
  };
};

module.exports = router => {
  router.post(
    '/api/v1/upload',
    passport.authenticate('bearer', config.jwt.session),
    upload.single('image'),
    async ctx => {
      try {
        const { buffer } = ctx.request.file;
        const fileStats = imageType(buffer);
        if (fileStats) {
          const { ext } = fileStats;
          const { user } = ctx.state;
          const id = user.id.toString();
          const processedObject = await processBuffer(buffer, id, ext);
          await B2.deleteFile(user.profileImageUrl);
          const url = await B2.uploadFile(processedObject);
          await user.update({
            profileImageUrl: url,
          });
          ctx.body = { result: 'Success' };
        } else {
          ctx.status = 400;
          ctx.body = { err: 'Unknown image format.' };
        }
      } catch (e) {
        logger.error(e);
        ctx.status = 500;
        ctx.body = { err: 'Internal error' };
      }
    }
  );
};
