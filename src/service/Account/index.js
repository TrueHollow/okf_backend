const jose = require('node-jose');
const uuidv4 = require('uuid/v4');
const KeyManager = require('../../common/KeyManager');
const { AccountModel } = require('../../db');

class Account {
  async create({ email, password, userName, profileImageUrl }) {
    if (
      typeof email !== 'string' ||
      typeof password !== 'string' ||
      typeof userName !== 'string'
    ) {
      return { err: 'Invalid data' };
    }
    const acc = await AccountModel.findOne({
      where: { email },
    });
    if (acc) {
      return { err: 'Already registered' };
    }
    const cleanData = {
      email,
      password,
      userName,
      profileImageUrl,
    };
    const account = await AccountModel.build(cleanData);
    await account.calcHashes(cleanData.password);
    await account.save();
    return { result: 'Account created' };
  }

  async login({ email, password }) {
    if (typeof email !== 'string' || typeof password !== 'string') {
      return { err: 'Invalid data' };
    }
    const acc = await AccountModel.findOne({
      where: { email },
    });
    if (!acc) {
      return { err: 'Email not found' };
    }
    const isValid = await acc.checkPassword(password);
    if (!isValid) {
      return { err: 'Invalid password' };
    }
    const tokenData = {
      user: {
        id: acc.id,
      },
      timestamp: new Date(),
    };
    const tokenString = JSON.stringify(tokenData);
    const key = await KeyManager.getKey();
    const token = await jose.JWE.createEncrypt({ format: 'compact' }, key)
      .update(tokenString)
      .final();
    return { result: { token } };
  }

  async update({ email, password, userName, profileImageUrl }, oldAccount) {
    if (
      typeof email !== 'string' ||
      typeof password !== 'string' ||
      typeof userName !== 'string'
    ) {
      return { err: 'Invalid data' };
    }
    const oldEmail = oldAccount.email.toLowerCase() === email.toLowerCase();
    if (!oldEmail) {
      const userExist = await AccountModel.findOne({
        where: { email },
      });
      if (userExist) {
        return { err: 'New email is in use.' };
      }
    }
    const hashPass = await oldAccount.calcHashes(password);
    const uid = uuidv4();
    await oldAccount.update({
      email,
      password: hashPass,
      userName,
      profileImageUrl,
      isActive: false,
      activationUid: uid,
    });
    return { result: { uid } };
  }

  async activate(uuid) {
    if (typeof uuid !== 'string') {
      return { err: 'Invalid data' };
    }
    const acc = await AccountModel.findOne({ where: { activationUid: uuid } });
    if (!acc) {
      return { err: 'Wrong activation code.' };
    }
    await acc.update({
      isActive: true,
      activationUid: null,
    });
    return { result: 'Activated' };
  }
}

module.exports = Account;
