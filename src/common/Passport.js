const PassportHttpBearer = require('passport-http-bearer');

const { Strategy } = PassportHttpBearer;
const jose = require('node-jose');
const { AccountModel } = require('../db');
const KeyManager = require('./KeyManager');
const logger = require('../logger')('src/common/Passport.js');

module.exports = passport => {
  passport.use(
    new Strategy(async (token, done) => {
      try {
        const key = await KeyManager.getKey();
        const joseJWE = await jose.JWE.createDecrypt(key).decrypt(token);
        const payloadString = joseJWE.payload.toString();
        const payload = JSON.parse(payloadString);

        const acc = await AccountModel.findByPk(payload.user.id);
        if (acc) {
          done(null, acc);
        } else {
          done(null, false);
        }
      } catch (e) {
        logger.warn(e);
        done('JWE error', false);
      }
    })
  );
};
