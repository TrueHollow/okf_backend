const B2 = require('backblaze-b2');
const config = require('../config');
const logger = require('../logger')('src/common/B2.js');

const { BACKBLAZE_KEY, BACKBLAZE_KEY_ID } = process.env;
if (!BACKBLAZE_KEY || !BACKBLAZE_KEY_ID) {
  logger.warn('You must set (BACKBLAZE_KEY, BACKBLAZE_KEY_ID) to use B2');
}

class B2Singleton {
  static async deleteFile(fileName) {
    if (!fileName) {
      return;
    }
    const b2 = new B2({
      applicationKeyId: BACKBLAZE_KEY_ID, // or accountId: 'accountId'
      applicationKey: BACKBLAZE_KEY, // or masterApplicationKey
    });
    const { bucketName } = config.b2;
    await b2.authorize();

    const resBucket = await b2.getBucket({
      bucketName,
    });

    const bucket = resBucket.data.buckets[0];
    const { bucketId } = bucket;

    const resListFileVersions = await b2.listFileVersions({
      bucketId,
      startFileName: fileName,
      maxFileCount: 1,
    });

    const { files } = resListFileVersions.data;
    if (Array.isArray(files) && files.length) {
      const { fileId } = files[0];
      await b2.deleteFileVersion({
        fileId,
        fileName,
      });
    }
  }

  static async uploadFile({ fileName, outputBuffer }) {
    const b2 = new B2({
      applicationKeyId: BACKBLAZE_KEY_ID, // or accountId: 'accountId'
      applicationKey: BACKBLAZE_KEY, // or masterApplicationKey
    });
    const { bucketName } = config.b2;

    const resAuthorize = await b2.authorize();
    const { downloadUrl } = resAuthorize.data;

    const resBucket = await b2.getBucket({
      bucketName,
    });

    const bucket = resBucket.data.buckets[0];
    const { bucketId } = bucket;

    const resUploadUrl = await b2.getUploadUrl({
      bucketId,
    });

    const { authorizationToken, uploadUrl } = resUploadUrl.data;

    await b2.uploadFile({
      uploadUrl,
      uploadAuthToken: authorizationToken,
      fileName,
      data: outputBuffer,
    });

    return `${downloadUrl}/file/${bucketName}/${fileName}`;
  }
}

module.exports = B2Singleton;
