const jose = require('node-jose');
const config = require('../config');

const { jwt } = config;

let key = null;

class KeyManager {
  static async getKey() {
    if (key === null) {
      key = await jose.JWK.asKey(jwt.key);
    }
    return key;
  }
}

module.exports = KeyManager;
