const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const passport = require('koa-passport');
const log4js = require('koa-log4');
const logger = require('../logger')('http');

const Passport = require('../common/Passport');

module.exports = app => {
  app.use(log4js.koaLogger(logger, { level: 'auto' }));
  app.use(cors());
  app.use(bodyParser());
  Passport(passport);
  app.use(passport.initialize());
  return passport;
};
