const fs = require('fs');
const path = require('path');
const chai = require('chai');
chai.use(require('chai-http'));
const { expect } = require('chai');
const faker = require('faker');
const logger = require('../../../src/logger')('test/api/v1/Account.test.js');
const { server } = require('../../../src/index');
const { AccountModel, sequelize } = require('../../../src/db');

const userData = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  userName: faker.internet.userName(),
};

let token;
let uid;
describe('Test Accounts http methods', () => {
  before(async () => {
    logger.info(`Test before. Fake data: (${JSON.stringify(userData)})`);
    return sequelize.sync();
  });
  after(async () => {
    logger.info('Test after');
    server.close();
    const acc = await AccountModel.findOne({
      where: { email: userData.email },
    });
    if (acc) {
      await acc.destroy({ force: true });
    }
  });
  it('test post method (Create Account)', async () => {
    const res = await chai
      .request(server)
      .post('/api/v1/account')
      .send(userData);
    expect(res).to.have.status(201);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.a.property('result')
      .and.not.to.have.a.property('err');
  });
  it('test post method (Login)', async () => {
    const res = await chai
      .request(server)
      .post('/api/v1/account/login')
      .send(userData);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.a.property('result')
      .and.not.to.have.a.property('err');
    expect(body.result.token).to.be.a('string');
    token = body.result.token;
  });
  it('test post method (upload image to Account profile)', async () => {
    const imagePath = path.resolve(__dirname, 'res', 'apple.png');
    const res = await chai
      .request(server)
      .post('/api/v1/upload')
      .attach('image', fs.readFileSync(imagePath), 'apple.png')
      .set('Authorization', `Bearer ${token}`);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.a.property('result')
      .and.not.to.have.a.property('err');
  });
  it('test get method (my Account)', async () => {
    const res = await chai
      .request(server)
      .get('/api/v1/account')
      .set('Authorization', `Bearer ${token}`);
    expect(res).to.have.status(200);
    const { body } = res;
    const copy = {
      userName: userData.userName,
      email: userData.email,
    };
    expect(body)
      .to.be.an('object')
      .and.to.include(copy);
  });
  it('test put method (update my Account)', async () => {
    userData.userName = faker.internet.userName();
    const res = await chai
      .request(server)
      .put('/api/v1/account')
      .set('Authorization', `Bearer ${token}`)
      .send(userData);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.a.property('result')
      .and.not.to.have.a.property('err');
    expect(body.result.uid).to.be.a('string');
    uid = body.result.uid;
  });
  it('test get method (activate my Account)', async () => {
    const url = `/api/v1/account/activate/${uid}`;
    const res = await chai.request(server).get(url);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body)
      .to.be.an('object')
      .and.to.have.a.property('result')
      .and.not.to.have.a.property('err');
  });
});
